### High Performance Computing
#### Autumn, 2018

#### Announcements (see [course webpage](https://imperialhpsc.bitbucket.io) for further information)

* 6/11/18: Homework 2 is online and can be found via the course webpage

* 23/10/18: Homework 1 is online and can be found via the course webpage

* 22/10/18: The posted coursework dates for M3C contained a few mistakes which have now been corrected: 1) The first coursework goes out tomorrow afternoon (Tuesday, 23/10)  and is due Friday, 2/11 (originally, this was 22/10-1/11). 2) Similar small adjustments have been made to the dates for the other assignments, the final correct dates are on the course webpage:  https://imperialm3c.bitbucket.io/assessment.html

* 11/10/18: I've set up a forum on Piazza to collect/discuss software problems. Please sign up [here:](https://piazza.com/imperial.ac.uk/fall2018/m3c) and participate! Posts there will get priority over emails.

* There will be an additional office hour on Monday, 8/10/18, 4-5pm, MLC if you need help with software installation or have general questions about the course

* Links for lecture 1 slides, introductory Python material, and course software installation instructions can all be found on the course webpage.


Codes, slides, exercises and solutions are (or will be) included in the course repo. Syncing your
fork and then updating your clone (using *git pull*) will give you your own copy
of all of these files.
